
DOCKER = docker
IMAGE_NODE       = node:12-stretch
IMAGE_SHELLCHECK = koalaman/shellcheck:stable
IMAGE_HADOLINT   = hadolint/hadolint:latest
IMAGE_FROM       = alpine:latest
IMAGE_BUILT      = robot-framework
#IMAGE_RF         = jfxs/robot-framework
IMAGE_RF         = robot-framework
IMAGE_SELENIUM   = selenium/standalone-chrome:3

## nodejs modules management
## -------------------------
yarn-install: ## Install nodejs modules with yarn. Arguments: [pull=n]
yarn-install:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache install

yarn-outdated: ## Check outdated npm packages. Arguments: [pull=n]
yarn-outdated:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache outdated || true

yarn-upgrade: ## Upgrade packages. Arguments: [pull=n]
yarn-upgrade:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache upgrade

clean-node-modules: ## Remove node_modules directory
clean-node-modules:
	rm -rf .node_cache
	rm -rf node_modules

.PHONY: yarn-install yarn-outdated yarn-upgrade clean-node-modules

## Docker
## ------
docker-build-push: ## Build docker image. Arguments: [arch=arm64] [vcs_ref=c780b3a] [tag=myTag]
docker-build-push: docker-rmi
	$(eval arch := $(shell if [ -z ${arch} ]; then echo "amd64"; else echo "${arch}"; fi))
	$(eval latest_sha := $(shell docker pull ${IMAGE_FROM} >/dev/null 2>&1 && docker inspect --format='{{index .RepoDigests 0}}' ${IMAGE_FROM}))
	@echo ${latest_sha}
	$(eval version := $(shell curl -s https://pypi.org/pypi/robotframework/json | jq -r '.info.version'))
	@echo ${version}
	$(eval mask_version := $(shell git fetch && git tag --sort=committerdate -l v* | tail -n1 | sed 's/^v\(.*\)$$/\1/'))
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "${IMAGE_BUILT}:latest"; else echo "${tag}"; fi))
	$(eval build_date := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ'))
	$(MAKE) set-version version="${mask_version}"
	if [ "${arch}" = "arm64" ]; then \
		docker buildx build --progress plain --no-cache --platform linux/arm64 --build-arg IMAGE_FROM_SHA="${latest_sha}" --build-arg RF_VERSION=${version} --build-arg MASK_VERSION=${mask_version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} --push . && \
		docker pull ${tag}; \
	else \
		docker build --no-cache --build-arg IMAGE_FROM_SHA=${latest_sha} --build-arg RF_VERSION=${version} --build-arg MASK_VERSION=${mask_version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} . && \
		docker push ${tag}; \
	fi

docker-rm: ## Remove all unused containers
docker-rm:
	$(DOCKER) container prune -f

docker-rmi: ## Remove all untagged images
docker-rmi: docker-rm
	$(DOCKER) image prune -f

PHONY: docker-build-push docker-rm docker-rmi

## Tests
## -----
checks: ## Run linter checks. Argument: [pull=n]
checks:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_SHELLCHECK} && \
		$(DOCKER) pull ${IMAGE_HADOLINT} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/mnt ${IMAGE_SHELLCHECK} files/*.sh
	$(DOCKER) run -t --rm -v ${PWD}:/mnt ${IMAGE_HADOLINT} hadolint /mnt/Dockerfile

rf-tests-simple: ## Run robot framework simple tests
rf-tests-simple:
	mkdir -p reports
	@if [ -z ${container} ]; then \
		chmod 777 reports && \
		$(DOCKER) run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/files:/files:ro -v ${PWD}/reports:/reports ${IMAGE_RF} robot --include simple --outputdir /reports -v TESTS_DIR:/tests -v SHELL_DIR:/files RF && \
		chmod 755 reports ; \
	else \
		robot --include simple --outputdir reports tests/RF ; \
	fi

tests-local: ## Run Robot Framework tests
tests-local:
	 $(DOCKER) network create tests-network
	 $(DOCKER) run --rm -d -p 4444:4444 -v /dev/shm:/dev/shm --network=tests-network --name selenium ${IMAGE_SELENIUM}
	 sleep 10
	 -$(DOCKER) run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/reports:/reports --network=tests-network ${IMAGE_BUILT} /bin/sh -c "pip install --user robotframework-faker && robot --include image --outputdir /reports RF"
	 $(DOCKER) kill selenium
	 $(DOCKER) network rm tests-network

clean-tests-local: ## Clean local test environment
clean-tests-local:
	 -$(DOCKER) kill selenium
	 -$(DOCKER) network rm tests-network

tests-remote: ## Run Robot Framework tests with a remote grid. Arguments: image=robot-framework, url=grid_url
tests-remote:
	test -n "${image}"  # Failed if image parameter is not set
	test -n "${url}"  # Failed if url parameter is not set
	$(DOCKER) run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/reports:/reports ${image} robot --include image -v GRID_URL:${url} --outputdir /reports RF

.PHONY: checks tests-local clean-tests-local tests-remote

## Admin
## -----
import-commit-key: ## Import key for sign commit
import-commit-key:
	mkdir -p .gnupg
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} gpg --import /root/${PRIVATE_KEY}
	chmod -R 700 .gnupg
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} gpg --list-secret-keys --keyid-format LONG
	@echo "Run command: git config user.signingkey XXXX"
	@echo "Run command: git config commit.gpgsign true"

commit: ## Commit with Commitizen command line. Arguments: [pull=n]
commit:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -it --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} /root/yarn-commit.sh

set-version: ## Set version in shell scripts. Arguments: version=2.8.1
set-version:
	test -n "${version}"  # Failed if version not set
	@for filename in files/*.sh; do \
		sed -i 's/__VERSION__/${version}/g' "$${filename}"; \
	done

.PHONY: import-commit-key commit set-version

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
